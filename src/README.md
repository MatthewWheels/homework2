#  Crazy Noise Orbs

![] (https://bitbucket.org/MatthewWheels/homework2/raw/7a0c0bdf1f49d0e50f504ddac8ba55bb1162cd98/Crazy_Orbs_Screenshot.png)

This program creates a simple array of 1000 orbs (using the primitive orb object) which are affected by noise in their:
- Position
- Radius

A *separate* noise equation controls the color of the background and the orb color.

Additional features:
- 2 Lights
- A camera (Controlled by mouse)

Created by Matthew Wheeler




### Code That This was Built Using

[professorColemanorbnoise](https://bitbucket.org/professorColeman/orbnoise/src/master/)

This program created a simple array of 1000 orbs (using the primitive orb object) which are affected by noise in their:
- Radius
- Brightness

Additional Features:
- 1 Warm Light
- 1 Cool Light
- 1 Camera (controllable by mouse)

Created by Professor Chris Coleman



