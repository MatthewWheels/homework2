#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

    //build a cube of objects with all three axis
       for(int x = 0; x< 10; x++){
           for(int y = 0; y< 10; y++){
               for(int z = 0; z< 10; z++){
                   int currOrb = ((x*10)+y)*10+z; //calculate the index for the orb
                   orbs[currOrb].setPosition(x*25, y*25, z*25); //position the orb
                   orbs[currOrb].setRadius(5); //set the size of the orbs
               }
           }
       }
       //Camera Position
       cam.setPosition(100, 100, 600);
       
       //setup a light
       light1.setDiffuseColor(ofColor(120, 250, 200));
       light1.setPosition(-100, 100, 500);
       light1.enable();
       
       //setup another light
       light2.setDiffuseColor(ofColor(115, 200, 230));
       light2.setPosition(-400, -200, 000);
       light2.enable();

}

//--------------------------------------------------------------
void ofApp::update(){
    noiseTime+=0.03; //move our timer forward
       //use our 3 axis grid again so we have the positions of each orb
       
       //calculates each RGB value of the background
       red=250*ofNoise(noiseTime/8);
       green=250*ofNoise(noiseTime/4);
       blue=250*ofNoise(noiseTime/16);
       //calculates the RGB values of the orbs
       orbRed=blue;
       orbGreen = red;
       orbBlue = green;
       
       for(int x = 0; x< 10; x++){
           for(int y = 0; y< 10; y++){
               for(int z = 0; z< 10; z++){
                   int currOrb = ((x*10)+y)*10+z; //calculate the index again
                   float myNoise = 25.0*ofSignedNoise(x/8.0+noiseTime/6, y/8.0-noiseTime/3, z/8.0, noiseTime/3); //figure out the noise at those coordinates
                   if(myNoise<0)myNoise = 0; //do not use the negative noise
                   orbs[currOrb].setRadius(myNoise); //change the size of the orb with the noise
                   
                   //changes the position of the orb with the noise
       orbs[currOrb].setPosition(x*25-(myNoise*2), y*25+(myNoise*2), z*25+(myNoise*2));
                   
               }
           }
       }
}

//--------------------------------------------------------------
void ofApp::draw(){
   
     ofBackground(red, green, blue);
     ofEnableDepthTest(); //sort the drawing so that the things closest to the camera is in front
     cam.begin();
     ofRotateDeg(45, 0, 1, 0); //rotate the cube 45 degrees on the y-axis
     
     for(int i = 0; i<1000; i++){
         ofSetColor(orbRed,orbGreen,orbBlue, (orbs[i].getRadius()*20));//set the color
        
         orbs[i].draw();//draw all the orbs
     }
    
     cam.end();
     ofDisableDepthTest();
  
     cam.draw(); //draw whatever the camera saw}
    
}
//--------------------------------------------------------------
